<?php
    session_start();

    function getParamGet($name) {
        return array_key_exists($name, $_GET) ? $_GET[$name] : null;
    }

    function getParamPost($name) {
        return array_key_exists($name, $_POST) ? $_POST[$name] : null;
    }

    function isPost() {
        return $_SERVER['REQUEST_METHOD'] == 'POST';
    }

    function redirect($page) {
        header('Location: ' . $page . '.php');
        exit;
    }

    function pathJoin($paths) {
        if(is_array($paths)) {
            $arrPaths = $paths;
            array_unshift($arrPaths, __DIR__, '..');
        } else {
            $arrPaths = [__DIR__, '..', (string) $paths];
        }
        return implode(DIRECTORY_SEPARATOR, $arrPaths);
    }

    function testIsValid($path) {
        // Возвращаем true в случае корректного теста, в противном случае описание ошибки
        $content = file_get_contents($path);
        if(!$content) {
            return "Не удалось получить содержимое файла";
        }
        $tests = json_decode($content, true);
        if(!is_array($tests)) {
            return "Файл с тестом должен быть json содержащим массив вопросов";
        }
        $importantFields = ["question", "answer", "correct"];
        $i = 1;
        $prefix = "Вопрос {$i}. ";
        foreach ($tests as $test) {
            foreach ($importantFields as $field) {
                if(!array_key_exists($field, $test)) {
                    return $prefix . "Отсутствует обязательное поле {$field}";
                }
            }
            if(!(is_string($test["question"])&&(is_string($test["correct"])||is_int($test["correct"])))) {
                return $prefix . "Поле question - должно содержать описание вопроса, а correct - ключ корректного ответа в массиве answer";
            }
            if(!(is_array($test["answer"])&&(array_key_exists($test["correct"], $test["answer"])))) {
                return $prefix . "В списке ответов на вопрос отсутствует ключ верного ответа указанный в поле correct";
            }
            $i++;
        }
        return true;
    }

?>