<?php
    require_once 'core/functions.php';

    $path = pathJoin(['tests', '']);
    $testName = getParamGet('name');
    if(empty($testName)) {
        $file = glob($path . "*.json")[0];
    } else {
        $file = $path . $testName. ".json";
    }

    if(!(file_exists($file)&&is_file($file))) {
        http_response_code(404);
        echo "<h1>Ошибка 404</h1>Нет такой страницы";
        exit;
    }

    $testName = basename($file, ".json");
    $json = file_get_contents($file);
    $test = json_decode($json, true);
    $tmpQuestion = '<h2>%3 Вопрос %1. %2</h2>';
    $tmpAnswer = '<input type="radio" name="%1" value="%2"><span style="%4">%3</span><br>';
    $tmpResult = '<p>Верных: %1 (из %2 вопросов)</p>';
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>PHP-19. Task 2.3</title>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
    <div class="nav">
        <a href="admin.php">Добавить тест</a>
        <a href="list.php">Выбрать тест</a>
        <a href="test.php">Пройти тест</a>
        <hr>
    </div>
    <form method="post">
        <label>
            Имя:
            <input type="text" name="name">
            <?php
                if (isPost() && empty(getParamPost('name'))) {
                    echo '<span style="color: red;">Вы забыли указать имя</span>';
                }
            ?>
        </label>
        <br>
        <h1>Тест <?php echo $testName; ?></h1>
        <?php
            $i = 1;
            $correctAnswer = 0;
            foreach ($test as $question) {
                $answerName = "answer" . $i;
                $questionResult = "";
                if (isPost()) {
                    if (getParamPost($answerName) == $question["correct"]) {
                        $questionResult = "&#10004;";
                        $correctAnswer++;
                    } else {
                        $questionResult = "&#10008;";
                    }
                }
                $tag = str_replace(["%1", "%2", "%3"], [$i, htmlspecialchars($question["question"]), $questionResult], $tmpQuestion);
                echo $tag;
                foreach ($question["answer"] as $key => $answer) {
                    if(isPost()) {
                        $styleAnswer = $key === $question["correct"] ? "color: green;": "color: black;";
                        $styleAnswer .= getParamPost($answerName) == $key ? "background-color: yellow;": "";
                    } else {
                        $styleAnswer = "";
                    }
                    echo str_replace(["%1", "%2", "%3", "%4"], [$answerName, htmlspecialchars($key), htmlspecialchars($answer), $styleAnswer], $tmpAnswer);
                }
                $i++;
            }
        ?>
        <hr>
        <input type="submit" value="Отправить">
    </form>

    <?php
        if(!isPost()) {
            exit;
        }

        $name = getParamPost('name');
        if(empty($name)) {
            http_response_code(400);
            exit;
        }

        $result = round($correctAnswer / count($test) * 100);
        if($result >= 90) {
            $score = 5;
        } elseif ($result >= 80) {
            $score = 4;
        } elseif ($result >= 60) {
            $score = 3;
        } else {
            $score = 2;
        }

        $_SESSION["name"] = $name;
        $_SESSION["score"] = $score;
        $_SESSION["result"] = $result;

        echo "<h2>Добрый день, " . $name . ". Ваш сертификат:</h2>";
        echo str_replace("%1", "certificate.php", '<img src="%1">');
    ?>

</body>
</html>

