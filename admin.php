<?php
    /*  Домашнее задание к лекции 2.3 «PHP и HTTP»

    Доработать предыдущее домашнее задание:
    1.  Добавить редирект на список тестов, который будет отрабатывать после загрузки нового теста.
    2.  При прохождении теста запрашивать имя (нужно дополнительное поле под имя). После прохождения теста генерировать PNG-сертификат с именем и оценкой.
    3.  При переходе на страницу теста с неправильным номером выдавать ошибку 404.


    Домашнее задание к лекции 2.2 «Обработка форм» (предыдущее)

    Генератор тестов на PHP и JSON:
        1. Создать файл admin.php с формой, через которую на сервер можно загрузить JSON-файл c тестом.
        2. Создать файл list.php со списком загруженных тестов.
        3. Создать файл test.php, который:
            - Принимает в качестве GET-параметра номер теста и отображает форму теста.
            - Если форма отправлена, проверяет и показывает результат.

    Структура теста
        - Структура JSON полностью на ваше усмотрение.
        - Тест — это несколько вопросов.
        - Вопрос — это текст вопроса, плюс несколько вариантов ответа. Один или несколько вариантов помечены как правильные.

    Пример теста:

    1. Сколько граммов в одном килограмме?
        - 10
        - 100
        - 1000
        - 10000

    2. Сколько метров в одном дециметре?
        - 100
        - 10
        - 0.1
        - 0.01

    */

    require_once 'core/functions.php';

    if(isset($_FILES) and array_key_exists("userfile", $_FILES)) {
        $file = $_FILES["userfile"];
        $result = testIsValid($file["tmp_name"]);
        if($result !== true) {
            echo $result;
            return;
        }

        $path = pathJoin(["tests", $file["name"]]);
        if(move_uploaded_file($file["tmp_name"], $path)) {
            header('Location: list.php');
            echo "Файл " . $file["name"] . " передан <br>";
        } else {
            echo "Файл не передан";
        }
    }
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>PHP-19. Task 2.3</title>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
    <div class="nav">
        <a href="admin.php">Добавить тест</a>
        <a href="list.php">Выбрать тест</a>
        <a href="test.php">Пройти тест</a>
        <hr>
    </div>
    <form method="post" enctype="multipart/form-data">
        <input type="file" name="userfile">
        <input type="submit" name="" value="Отправить">
    </form>
</body>
</html>
